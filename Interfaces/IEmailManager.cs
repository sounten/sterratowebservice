﻿using System.Threading.Tasks;

namespace SterratoWebService.Interfaces
{
    public interface IEmailManager
    {
        Task SendMailAsync(string subject, string body);
    }
}