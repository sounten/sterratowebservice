﻿using Amazon.DynamoDBv2.DocumentModel;
using SterratoWebService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SterratoWebService.Interfaces
{
    public interface ISchedulingModule
    {
        Task<List<Document>> GetScheduleAsDocumentList(string dayId);
        Task<string> GetScheduleAsJson(string dayId);
        Task<int> UpdateDefaultSchedule(string dayOfWeek, List<FranjaTrabajo> openingPeriods);
        Task<string> UpdateSchedule(string dayId, List<FranjaTrabajo> openingPeriods);
    }
}