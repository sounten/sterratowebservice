﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SterratoWebService.Interfaces
{
    public interface INewsletterModule
    {
        Task<List<string>> GetSubscriptions();
        Task AddSubscription(string email);
        Task RemoveSubscription(string email);
    }
}
