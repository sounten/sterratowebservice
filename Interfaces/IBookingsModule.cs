﻿using SterratoWebService.Models;
using System.Threading.Tasks;

namespace SterratoWebService.Interfaces
{
    public interface IBookingsModule
    {
        Task<string> GetBookingAsJsonAsync(string dayId);
        Task<bool> AddBookingAsync(string dayId, BookingRequest booking);
    }
}