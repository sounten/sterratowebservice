﻿using SterratoWebService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SterratoWebService.Interfaces
{
    public interface IServicesModule
    {
        Task<List<Servicio>> GetServices();
    }
}
