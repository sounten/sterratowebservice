﻿using Microsoft.AspNetCore.Mvc;
using SterratoWebService.Interfaces;
using System.Threading.Tasks;

namespace SterratoWebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewsletterController : ControllerBase
    {
        private readonly INewsletterModule _module;

        public NewsletterController(INewsletterModule module)
        {
            _module = module;
        }

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            Response.Headers.Add("Access-Control-Allow-Origin", "*");
            Response.Headers.Add("Access-Control-Allow-Headers", "*");
            Response.Headers.Add("Access-Control-Allow-Methods", "DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT");

            var result = await _module.GetSubscriptions();

            return Ok(result);
        }

        [HttpPost("subscriptions/{email}")]
        public async Task<ActionResult> Post(string email)
        {
            Response.Headers.Add("Access-Control-Allow-Origin", "*");
            Response.Headers.Add("Access-Control-Allow-Headers", "*");
            Response.Headers.Add("Access-Control-Allow-Methods", "DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT");

            await _module.AddSubscription(email);

            return Ok();
        }

        [HttpDelete("subscriptions/{email}")]
        public async Task<ActionResult> Delete(string email)
        {
            Response.Headers.Add("Access-Control-Allow-Origin", "*");
            Response.Headers.Add("Access-Control-Allow-Headers", "*");
            Response.Headers.Add("Access-Control-Allow-Methods", "DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT");

            await _module.RemoveSubscription(email);

            return Ok();
        }
    }
}
