﻿using Microsoft.AspNetCore.Mvc;
using SterratoWebService.Interfaces;
using SterratoWebService.Models;
using System;
using System.Threading.Tasks;

namespace SterratoWebService.Controllers
{
    [Route("api/horarios")]
    [ApiController]
    public class SchedulingController : ControllerBase
    {
        private readonly ISchedulingModule _module;

        public SchedulingController(ISchedulingModule module)
        {
            _module = module;
        }

        [HttpGet("{dayId}")]
        public async Task<ActionResult> Get(string dayId)
        {
            try
            {
                Response.Headers.Add("Access-Control-Allow-Origin", "*");
                Response.Headers.Add("Access-Control-Allow-Headers", "*");
                Response.Headers.Add("Access-Control-Allow-Methods", "DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT");

                var result = await _module.GetScheduleAsJson(dayId);

                return Ok(result);
            }
            catch (Exception e)
            {
                Console.Write(e.StackTrace);
                return BadRequest(e);
            }
        }

        [HttpPost("default/{dayOfWeek}")]
        public async Task<ActionResult> PostDefault(string dayOfWeek, [FromBody] ScheduleUpdate scheduleUpdate)
        {
            try
            {
                Response.Headers.Add("Access-Control-Allow-Origin", "*");
                Response.Headers.Add("Access-Control-Allow-Headers", "*");
                Response.Headers.Add("Access-Control-Allow-Methods", "DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT");

                var result = await _module.UpdateDefaultSchedule(dayOfWeek, scheduleUpdate.Schedule);

                if ((result >= 200) && (result <= 299))
                {

                    return Ok(new { DynamoDBResult = result });
                }
                else
                    return StatusCode(result);
            }
            catch (Exception e)
            {
                Console.Write(e.StackTrace);
                return BadRequest(e);
            }
        }

        [HttpPost("{dayId}")]
        public async Task<ActionResult> PostDaySchedule(string dayId, [FromBody] ScheduleUpdate scheduleUpdate)
        {
            try
            {
                Response.Headers.Add("Access-Control-Allow-Origin", "*");
                Response.Headers.Add("Access-Control-Allow-Headers", "*");
                Response.Headers.Add("Access-Control-Allow-Methods", "DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT");

                var result = await _module.UpdateSchedule(dayId, scheduleUpdate.Schedule);

                if (result != null)
                    return Ok(result);
                else
                    return BadRequest(new { Error = "DynamoDB response was null." });
            }
            catch (Exception e)
            {
                Console.Write(e.StackTrace);
                return BadRequest(e);
            }
        }
    }
}
