﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using SterratoWebService.Configuration;

namespace SterratoWebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MonitorController : ControllerBase
    {
        private readonly EmailConfig emailConfig;
        private readonly DynamoDbConfig dynamoDbConfig;

        public MonitorController(IOptions<EmailConfig> emailConf, IOptions<DynamoDbConfig> dynDbConf)
        {
            emailConfig = emailConf.Value;
            dynamoDbConfig = dynDbConf.Value;
        }

        [HttpGet("ping")]
        public ActionResult Ping()
        {
            Response.Headers.Add("Access-Control-Allow-Origin", "*");
            Response.Headers.Add("Access-Control-Allow-Headers", "*");
            Response.Headers.Add("Access-Control-Allow-Methods", "DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT");

            return Ok(new { Pong = "true" });
        }

        [HttpGet("environmentvariables")]
        public ActionResult EnvVars()
        {
            Response.Headers.Add("Access-Control-Allow-Origin", "*");
            Response.Headers.Add("Access-Control-Allow-Headers", "*");
            Response.Headers.Add("Access-Control-Allow-Methods", "DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT");

            return Ok(new { emailConfig.ServiceEmailAddress, DynamoDbName = dynamoDbConfig });
        }
    }
}
