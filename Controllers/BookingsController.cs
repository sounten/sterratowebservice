﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SterratoWebService.Interfaces;
using SterratoWebService.Models;
using System;
using System.Threading.Tasks;

namespace SterratoWebService.Controllers
{
    [Route("api/reservas")]
    [ApiController]
    public class BookingsController : ControllerBase
    {
        private readonly IBookingsModule _module;
        private readonly IEmailManager _mailingService;

        public BookingsController(IBookingsModule module, IEmailManager mailingService)
        {
            _module = module;
            _mailingService = mailingService;
        }

        [HttpGet("{dayId}")]
        public async Task<ActionResult<string>> Get(string dayId)
        {
            try
            {
                Response.Headers.Add("Access-Control-Allow-Origin", "*");
                Response.Headers.Add("Access-Control-Allow-Headers", "*");
                Response.Headers.Add("Access-Control-Allow-Methods", "DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT");

                var result = await _module.GetBookingAsJsonAsync(dayId);

                return Ok(result);
            }
            catch (Exception e)
            {
                Console.Write(e);
                return UnprocessableEntity();
            }
        }

        [HttpPost("{dayId}")]
        public async Task<ActionResult> Post(string dayId, [FromBody] BookingRequest bookingRequest)
        {
            try
            {
                Response.Headers.Add("Access-Control-Allow-Origin", "*");
                Response.Headers.Add("Access-Control-Allow-Headers", "*");
                Response.Headers.Add("Access-Control-Allow-Methods", "DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT"); 

                var result = await _module.AddBookingAsync(dayId, bookingRequest);

                if (result)
                    return Ok();
                else
                    return BadRequest();
            }
            catch (Exception e)
            {
                Console.Write(e);
                return UnprocessableEntity();
            }
        }
    }
}
