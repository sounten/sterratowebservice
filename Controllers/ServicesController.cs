﻿using Microsoft.AspNetCore.Mvc;
using SterratoWebService.Interfaces;
using System.Threading.Tasks;

namespace SterratoWebService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServicesController : ControllerBase
    {
        private readonly IServicesModule _module;

        public ServicesController(IServicesModule module)
        {
            _module = module;
        }

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            Response.Headers.Add("Access-Control-Allow-Origin", "*");
            Response.Headers.Add("Access-Control-Allow-Headers", "*");
            Response.Headers.Add("Access-Control-Allow-Methods", "DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT");

            var result = await _module.GetServices();

            return Ok(new { Services = result });
        }
    }
}
