﻿namespace SterratoWebService.Models
{
    public class Servicio
    {
        public string Nombre { get; set; }
        public string Precio { get; set; }
        public string Duracion { get; set; }
    }
}