﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SterratoWebService.Models
{
    public class ScheduleUpdate
    {
        public List<FranjaTrabajo> Schedule { get; set; }
    }
}
