﻿namespace SterratoWebService.Models
{
    public class Booking
    {
        public string DayId { get; set; }
        public string Time { get; set; }
        public string Service { get; set; }
        public string Duration { get; set; }
    }
}