﻿using Amazon.DynamoDBv2.DataModel;
using System.Collections.Generic;

namespace SterratoWebService.Models
{
    public class NewsletterSubscriptions
    {
        [DynamoDBIgnore]
        public static string staticId = "NewsletterSubscriptions";

        [DynamoDBHashKey]
        public string Id
        {
            // Attempt to set up object mapping so that there will only ever be one database instance of this class.
            // Static field might work on its own but no time to check.
            get => staticId;
            set => _ = value;
        }
        public List<string> Subscriptions { get; set; }
    }
}
