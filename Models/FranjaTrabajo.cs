﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SterratoWebService.Models
{
    public class FranjaTrabajo
    {
        public string Abre { get; set; }
        public string Cierra { get; set; }
    }
}
