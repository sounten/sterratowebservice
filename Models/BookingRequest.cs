﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SterratoWebService.Models
{
    public class BookingRequest
    {
        public string DayId { get; set; }
        public string Time { get; set; }
        public string Service { get; set; }
        public string Duration { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Comment { get; set; }
    }
}
