﻿using Amazon.DynamoDBv2.DataModel;
using Microsoft.Extensions.Options;
using SterratoWebService.Configuration;
using SterratoWebService.Interfaces;
using SterratoWebService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SterratoWebService.Concrete
{
    public class ServicesModule : IServicesModule
    {
        private readonly IDynamoDBContext _context;
        private readonly DynamoDbConfig _config;

        public ServicesModule(IDynamoDBContext context, IOptionsMonitor<DynamoDbConfig> config)
        {
            _config = config.CurrentValue;
            _context = context;
        }

        public async Task<List<Servicio>> GetServices()
        {
            var result = await _context.LoadAsync<Services>(Services.staticId, new DynamoDBOperationConfig
            {
                OverrideTableName = _config.Table
            });

            return result.Servicios;
        }
    }
}
