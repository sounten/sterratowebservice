﻿using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Microsoft.Extensions.Options;
using SterratoWebService.Configuration;
using SterratoWebService.Extensions;
using SterratoWebService.Interfaces;
using SterratoWebService.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace SterratoWebService.Concrete
{
    public class SchedulingModule : ISchedulingModule
    {
        private readonly IAmazonDynamoDB _dynamoDb;
        private readonly IDynamoDBContext _context;
        private readonly DynamoDbConfig _config;

        public SchedulingModule(IAmazonDynamoDB dynamoDb, IDynamoDBContext context,
            IOptionsMonitor<DynamoDbConfig> dynamoDbConfig)
        {
            _dynamoDb = dynamoDb;
            _context = context;
            _config = dynamoDbConfig.CurrentValue;
        }

        public async Task<List<Document>> GetScheduleAsDocumentList(string dayId)
        {
            var date = DateTime.ParseExact(dayId, "yyyyMMdd", CultureInfo.InvariantCulture);

            if (date.IsCatalanPublicHoliday())
                return null;

            var result = await GetDefaultScheduleAsDocument(date);

            result.TryGetValue("Horario", out DynamoDBEntry schedule);
            return schedule?.AsListOfDocument();
        }

        private async Task<Document> GetDefaultScheduleAsDocument(DateTime date)
        {
            var table = Table.LoadTable(_dynamoDb, _config.Table);

            var result = await table.GetItemAsync(new Primitive(date.DayOfWeek("es-ES", true)));
            var isnull = result == null;

            return result;
        }

        public async Task<int> UpdateDefaultSchedule(string dayOfWeek, List<FranjaTrabajo> openingPeriods)
        {
            // Build AttributeValue list of OpeningPeriods
            var attrValues = new List<AttributeValue>();
            foreach (var period in openingPeriods)
            {
                var periodAttrValue = new AttributeValue
                {
                    M = new Dictionary<string, AttributeValue>
                    {
                        { "Abre", new AttributeValue { S = period.Abre } },
                        { "Cierra", new AttributeValue { S = period.Cierra } },
                    }
                };

                attrValues.Add(periodAttrValue);
            }

            var table = Table.LoadTable(_dynamoDb, _config.Table);

            // Create update request
            var request = new UpdateItemRequest
            {
                TableName = _config.Table,
                Key = new Dictionary<string, AttributeValue>
                {
                    { "Id", new AttributeValue { S = dayOfWeek } }
                },
                AttributeUpdates = new Dictionary<string, AttributeValueUpdate>
                {
                    {
                        "Horario", new AttributeValueUpdate
                        {
                            Action = "ADD",
                            Value = new AttributeValue
                            {
                                L = attrValues
                            }
                        }
                    }
                },
            };

            // Upsert to DynamoDB
            var response = await _dynamoDb.UpdateItemAsync(request);

            return (int) response.HttpStatusCode;
        }

        public async Task<string> GetScheduleAsJson(string dayId)
        {
            var date = DateTime.ParseExact(dayId, "yyyyMMdd", CultureInfo.InvariantCulture);

            if (date.IsCatalanPublicHoliday())
                return null;

            var result = await GetDefaultScheduleAsDocument(date);

            return result.ToJson();
        }

        public async Task<string> UpdateSchedule(string dayId, List<FranjaTrabajo> schedule)
        {
            var table = Table.LoadTable(_dynamoDb, _config.Table);
            
            var scheduleAsDocumentList = new List<Document>();
            foreach (var period in schedule)
            {
                scheduleAsDocumentList.Add(_context.ToDocument(period));
            }

            var dayDocument = new Document();
            dayDocument["Id"] = dayId;
            dayDocument["Horario"] = scheduleAsDocumentList;

            var resultingDocument = await table.UpdateItemAsync(dayDocument);

            return resultingDocument?.ToJson();
        }
    }
}