﻿using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Microsoft.Extensions.Options;
using SterratoWebService.Configuration;
using SterratoWebService.Interfaces;
using SterratoWebService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SterratoWebService.Concrete
{
    public class NewsletterModule : INewsletterModule
    {
        private readonly IDynamoDBContext _context;
        private readonly DynamoDbConfig _config;

        public NewsletterModule(IDynamoDBContext context, IOptionsMonitor<DynamoDbConfig> config)
        {
            _context = context;
            _config = config.CurrentValue;
        }

        public async Task<List<string>> GetSubscriptions()
        {
            var newsletterSubscriptions = await _context
                .LoadAsync<NewsletterSubscriptions>(NewsletterSubscriptions.staticId, new DynamoDBOperationConfig
                {
                    OverrideTableName = _config.Table
                });

            return newsletterSubscriptions.Subscriptions;
        }

        public async Task AddSubscription(string email)
        {
            var newsletterSubscriptions = await _context
                .LoadAsync<NewsletterSubscriptions>(NewsletterSubscriptions.staticId, new DynamoDBOperationConfig
                {
                    OverrideTableName = _config.Table
                });

            if (newsletterSubscriptions != null)
            {
                newsletterSubscriptions.Subscriptions?.Add(email);

                await _context.SaveAsync(newsletterSubscriptions, new DynamoDBOperationConfig
                {
                    OverrideTableName = _config.Table
                });
            }
            else
            {
                await _context.SaveAsync(new NewsletterSubscriptions
                {
                    Subscriptions = new List<string>()
                },
                new DynamoDBOperationConfig
                {
                    OverrideTableName = _config.Table
                });
            }
        }

        public async Task RemoveSubscription(string email)
        {
            var newsletterSubscriptions = await _context
                .LoadAsync<NewsletterSubscriptions>(NewsletterSubscriptions.staticId, new DynamoDBOperationConfig
                {
                    OverrideTableName = _config.Table
                });

            if (newsletterSubscriptions != null)
            {
                newsletterSubscriptions.Subscriptions?.Remove(email);

                await _context.SaveAsync(newsletterSubscriptions, new DynamoDBOperationConfig
                {
                    OverrideTableName = _config.Table
                });
            }
        }
    }
}
