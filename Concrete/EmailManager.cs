﻿using Amazon;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Microsoft.Extensions.Options;
using SterratoWebService.Configuration;
using SterratoWebService.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SterratoWebService.Concrete
{
    public class EmailManager : IEmailManager
    {
        private readonly EmailConfig _config;

        public EmailManager(IOptionsMonitor<EmailConfig> config)
        {
            _config = config.CurrentValue;
        }

        public async Task SendMailAsync(string subject, string body)
        {
            //using (var smtpClient = new SmtpClient
            //{
            //    Host = _config.SmtpHost,
            //    Port = _config.SmtpPort,
            //    Credentials = new NetworkCredential
            //    {
            //        UserName = _config.ServiceEmailAddress,
            //        Password = _config.ServiceEmailPassword
            //    },
            //    EnableSsl = true
            //})
            //{
            //    await smtpClient.SendMailAsync(new MailMessage(_config.ServiceEmailAddress, _config.RecipientEmailAddress, subject, body));
            //}

            using (var client = new AmazonSimpleEmailServiceClient(RegionEndpoint.EUWest1))
            {
                var sendRequest = new SendEmailRequest
                {
                    Source = _config.ServiceEmailAddress,
                    Destination = new Destination
                    {
                        ToAddresses =
                        new List<string> { _config.RecipientEmailAddress }
                    },
                    Message = new Message
                    {
                        Subject = new Content(subject),
                        Body = new Body
                        {
                            Text = new Content
                            {
                                Charset = "UTF-8",
                                Data = body
                            }
                        }
                    },
                    //ConfigurationSetName = _config.ConfigSet
                };
                try
                {
                    Console.WriteLine("Sending email using Amazon SES...");
                    var res = await client.SendEmailAsync(sendRequest);
                    Console.WriteLine("Send Message result: " + res.HttpStatusCode);
                    Console.WriteLine("The email was sent successfully.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("The email was not sent.");
                    Console.WriteLine("Error message: " + ex.Message);
                }
            }
        }
    }
}
