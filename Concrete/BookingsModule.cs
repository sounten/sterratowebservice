﻿using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Microsoft.Extensions.Options;
using SterratoWebService.Configuration;
using SterratoWebService.Extensions;
using SterratoWebService.Interfaces;
using SterratoWebService.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SterratoWebService.Concrete
{
    public class BookingsModule : IBookingsModule
    {
        private readonly IAmazonDynamoDB _dynamoDb;
        private readonly ISchedulingModule _schedulingModule;
        private readonly IEmailManager _emailManager;
        private readonly DynamoDbConfig _config;

        public BookingsModule(IAmazonDynamoDB dynamoDb, ISchedulingModule schedulingModule,
            IEmailManager emailManager, IOptionsMonitor<DynamoDbConfig> dynamoDbConfig)
        {
            _dynamoDb = dynamoDb;
            _schedulingModule = schedulingModule;
            _emailManager = emailManager;
            _config = dynamoDbConfig.CurrentValue;
        }

        public async Task<string> GetBookingAsJsonAsync(string dayId)
        {
            var table = Table.LoadTable(_dynamoDb, _config.Table);

            var result = await table.GetItemAsync(new Primitive(dayId.ToString()));

            if (result == null || !result.GetAttributeNames().Contains("Horario"))
            {
                result = result ?? new Document();

                var schedule = await _schedulingModule.GetScheduleAsDocumentList(dayId);

                if (schedule != null)
                    result["Horario"] = schedule;
            }

            return result.ToJson();
        }
        
        /// <returns>HttpStatusCode returned from the update request sent to DynamoDB, as an <c>Int32</c>.</returns>
        public async Task<bool> AddBookingAsync(string dayId, BookingRequest bookingRequest)
        {
            // THIS IS LOW LEVEL SYNTAX! For simple operations it's better to use DynamoDBContext. /!\
            // DynamoDBContext also allows saving/loading .NET C# objects to/from DynamoDB.
            // Create update request:

            var request = new UpdateItemRequest
            {
                TableName = _config.Table,
                Key = new Dictionary<string, AttributeValue>
                {
                    { "Id", new AttributeValue { S = dayId } }
                },
                AttributeUpdates = new Dictionary<string, AttributeValueUpdate>
                {
                    {
                        "Reservas", new AttributeValueUpdate
                        {
                            Action = "ADD",
                            Value = new AttributeValue
                            {
                                L = new List<AttributeValue>
                                {
                                    new AttributeValue
                                    {
                                        M = new Dictionary<string, AttributeValue>
                                        {
                                            { "Hora", new AttributeValue { S = bookingRequest.Time } },
                                            { "Servicio", new AttributeValue { S = bookingRequest.Service } },
                                            { "Duracion", new AttributeValue { N = bookingRequest.Duration } }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
            };

            // Upsert to DynamoDB:
            var response = await _dynamoDb.UpdateItemAsync(request);

            // Result handling
            var result = (int)response.HttpStatusCode;
            var success = (result >= 200) && (result <= 299);

            if (success)
            {
                var subject = "Reserva en SterratoCicliWebService";

                var body = $"{bookingRequest.Name} ha reservado el servicio {bookingRequest.Service} " +
                    $"con duración de {bookingRequest.Duration} minutos.{Environment.NewLine}" +
                    $"{Environment.NewLine}Detalles:{Environment.NewLine}" +
                    $"    Fecha: {bookingRequest.DayId.AsFormattedDate("dd/MM/yyyy")}{Environment.NewLine}" +
                    $"    Hora:  {bookingRequest.Time}{Environment.NewLine}" +
                    $"{Environment.NewLine}Contacto:{Environment.NewLine}" +
                    $"    Teléfono: {bookingRequest.Phone}{Environment.NewLine}" +
                    $"    Email:    {bookingRequest.Email}{Environment.NewLine}" +
                    $"{Environment.NewLine}Comentarios:{Environment.NewLine}" +
                    $"    {bookingRequest.Comment}";

                await _emailManager.SendMailAsync(subject, body);
            }

            return success;
        }
    }
}