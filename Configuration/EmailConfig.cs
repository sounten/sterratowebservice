﻿namespace SterratoWebService.Configuration
{
    public class EmailConfig
    {
        public string ServiceEmailAddress { get; set; }
        public string RecipientEmailAddress { get; set; }
    }
}