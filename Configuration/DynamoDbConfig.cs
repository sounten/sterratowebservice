﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SterratoWebService.Configuration
{
    public class DynamoDbConfig
    {
        public string Table { get; set; }
        public string TypeIndex { get; set; }
    }
}
