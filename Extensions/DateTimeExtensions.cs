﻿using Nager.Date;
using System;
using System.Globalization;

namespace SterratoWebService.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool IsCatalanPublicHoliday(this DateTime date) => 
            DateSystem.IsOfficialPublicHolidayByCounty(date, CountryCode.ES, "ES-CT");

        public static string DayOfWeek(this DateTime date, string cultureName, bool longName) =>
            date.ToString(longName ? "dddd" : "ddd", new CultureInfo(cultureName)).ToLower();
    }
}
