﻿using System;
using System.Globalization;

namespace SterratoWebService.Extensions
{
    public static class DayIdStringExtensions
    {
        public static DateTime AsDateTime(this string s) =>
            DateTime.ParseExact(s, "yyyyMMdd", CultureInfo.InvariantCulture);

        public static string AsDayName(this string s, string cultureName, bool longName) =>
            s.AsDateTime().DayOfWeek(cultureName, longName);

        public static string AsFormattedDate(this string s, string format) =>
            s.AsDateTime().ToString(format);
    }
}
