﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SterratoWebService.Concrete;
using SterratoWebService.Configuration;
using SterratoWebService.Interfaces;
using System;

namespace SterratoWebService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Amazon stuff:
            services.AddDefaultAWSOptions(Configuration.GetAWSOptions());
            services.AddAWSService<IAmazonDynamoDB>();
            services.AddTransient<IDynamoDBContext, DynamoDBContext>();

            // Our stuff:
            services.AddScoped<IBookingsModule, BookingsModule>();
            services.AddScoped<ISchedulingModule, SchedulingModule>();
            services.AddScoped<INewsletterModule, NewsletterModule>();
            services.AddScoped<IServicesModule, ServicesModule>();

            services.AddTransient<IEmailManager, EmailManager>();

            services.Configure<EmailConfig>(config =>
            {
                config.ServiceEmailAddress = Environment.GetEnvironmentVariable("WebServiceEmailAddress");
                config.RecipientEmailAddress = Environment.GetEnvironmentVariable("RecipientEmailAddress");
            });

            services.Configure<DynamoDbConfig>(config =>
            {
                config.Table = Environment.GetEnvironmentVariable("DynamoDbTableName");
                config.TypeIndex = Environment.GetEnvironmentVariable("TypeIndexName");
            });
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
